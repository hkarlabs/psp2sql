from django.apps import AppConfig


class Psp2SqlConfig(AppConfig):
    name = 'psp2sql'
