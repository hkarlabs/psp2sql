from django.db import models


class Person(models.Model):
    name_prefix = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    name_suffix = models.CharField(max_length=50)
    sex = models.CharField(max_length=1)
    birthdate = models.DateField()
    last_change = models.DateField()
    death = models.DateField()


class BodiesType(models.Model):
    parent = models.ForeignKey('self', on_delete=models.CASCADE)
    name_cz = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100)
    type = models.ForeignKey('self', related_name='%(class)s_common_type', on_delete=models.CASCADE)
    priority = models.PositiveSmallIntegerField()


class FunctionType(models.Model):
    name_cz = models.CharField(max_length=60)
    name_en = models.CharField(max_length=60)
    authority_type = models.ForeignKey(BodiesType, on_delete=models.CASCADE)
    priority = models.PositiveSmallIntegerField()
    type = models.PositiveSmallIntegerField()


class Function(models.Model):
    authority_type = models.ForeignKey(BodiesType, on_delete=models.CASCADE)
    type = models.ForeignKey(FunctionType, on_delete=models.CASCADE)
    internal_name_cz = models.CharField(max_length=100)
    priority = models.PositiveSmallIntegerField()


class Bodies(models.Model):
    parent = models.ForeignKey('self', on_delete=models.CASCADE)
    authority_type = models.ForeignKey(BodiesType, on_delete=models.CASCADE)
    abbreviation = models.CharField(max_length=8)
    name_cz = models.CharField(max_length=500)
    name_en = models.CharField(max_length=500)
    since = models.DateField()
    to = models.DateField()
    priority = models.PositiveSmallIntegerField()


class Belonging(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    membership = models.ForeignKey(Bodies, on_delete=models.CASCADE)
    function = models.ForeignKey(Function, on_delete=models.CASCADE)
    is_function = models.BooleanField()
    since = models.DateTimeField()
    to = models.DateTimeField()
    mandate_from = models.DateField()
    mandate_to = models.DateField()


class Deputy(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    region = models.ForeignKey(Bodies, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Bodies, related_name='%(class)s_candidate', on_delete=models.CASCADE)
    election_period = models.ForeignKey(Bodies, related_name='%(class)s_election_period', on_delete=models.CASCADE)
    web = models.CharField(max_length=100)
    street = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    zip = models.CharField(max_length=5)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    fax = models.CharField(max_length=50)
    fixed_line = models.CharField(max_length=20)
    facebook = models.CharField(max_length=100)
    photo = models.BooleanField()
